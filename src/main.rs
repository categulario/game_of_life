use std::path::{Path, PathBuf};
use std::fs::File;
use std::io::{BufRead, BufReader};

use nannou::prelude::*;
use clap::Parser;

#[derive(Copy, Clone)]
enum CellType {
    Alive,
    Dead,
}

fn alive_neighbours(data:&[CellType], index:usize, width:u32) -> u32 {
    let cur_row = ((index as u32)/width) as i32;
    let cur_col = ((index as u32)%width) as i32;
    let mut count = 0;

    for r in (cur_row-1)..(cur_row+2) {
        for c in (cur_col-1)..(cur_col+2) {
            let i = r*(width as i32) + c;

            if i>=0 && i < (data.len() as i32) && i != (index as i32) {
                count += match data[i as usize] {
                    CellType::Alive => 1,
                    _ => 0,
                }
            }
        }
    }

    count
}

fn data_from_file<T: AsRef<Path>>(filename: T, width:u32, height:u32) -> Vec<CellType> {
    let f = File::open(filename).expect("Unable to open file");
    let f = BufReader::new(f);
    let mut arr = vec![CellType::Dead; (width*height) as usize];

    for (h, line) in f.lines().enumerate() {
        if (h as u32) == height {
            break;
        }

        let line = line.expect("Something happened while reading a line from the file");

        for (w, c) in line.chars().enumerate() {
            if (w as u32) == width {
                break;
            }

            if c == 'x' {
                arr[h*(width as usize) + w] = CellType::Alive;
            }
        }
    }

    arr
}



#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// Width of the grid
    #[arg(short, long, default_value_t=48)]
    width: u32,

    /// Height of the grid
    #[arg(long, default_value_t=48)]
    height: u32,

    /// Input file
    #[arg(short, long, value_name = "FILE")]
    file: PathBuf,

    /// Delay in fractional seconds
    #[arg(short, long, default_value_t=1.0)]
    delay: f64,
}

struct Model {
    data: Vec<CellType>,
    width: u32,
    seconds: f64,
    delay: f64,
}

fn main() {
    nannou::app(model)
        .update(update)
        .simple_window(view)
        .run();
}

fn model(_app: &App) -> Model {
    let cli = Cli::parse();
    let data = data_from_file(cli.file, cli.width, cli.height);

    Model {
        data,
        width: 30,
        seconds: 0.0,
        delay: cli.delay,
    }
}

fn update(_app: &App, model: &mut Model, update: Update) {
    model.seconds += update.since_last.as_secs_f64();

    if model.seconds < model.delay {
        return;
    }

    let mut newdata = Vec::new();
    let data = &model.data;

    for (index, cell) in data.iter().enumerate() {
        newdata.push(match *cell {
            CellType::Alive => match alive_neighbours(data, index, model.width) {
                0..=1 => CellType::Dead, // underpopulation
                2..=3 => CellType::Alive, // normal population
                4..=8 => CellType::Dead, // overpopulation
                _ => CellType::Dead, // this doesn't really happen...
            },
            CellType::Dead => match alive_neighbours(data, index, model.width) {
                3 => CellType::Alive,
                _ => CellType::Dead,
            },
        });
    }

    model.data = newdata;
    model.seconds = 0.0;
}

fn view(app: &App, model: &Model, frame: Frame) {
    let data = &model.data;
    let width = model.width;

    let draw = app.draw();
    draw.background().color(WHITE);

    for h in 0..(data.len() as u32)/width {
        for w in 0..width {
            if let CellType::Alive = data[(h*width + w) as usize] {
                draw.rect()
                    .x_y((w*10) as f32, (h*10) as f32)
                    .w(10.0)
                    .h(10.0)
                    .color(BLACK);
            }
        }
    }

    draw.to_frame(app, &frame).unwrap();
}
